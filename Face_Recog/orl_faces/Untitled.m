%%
%Author: Abhijit Acharya
%
%%
%Add path of toolboxes
addpath D:\Navrachana\ToolboxesMatlab\utility
addpath D:\Navrachana\ToolboxesMatlab\machineLearning
addpath D:\Navrachana\ToolboxesMatlab\sap
addpath D:\Navrachana\ToolboxesMatlab\asr
%%
clc
close all
clear all
%%
%Read Audio
fs=16000; % Sampling rate
nbits=16;
nChannels=1;
duration=3; % Recording duration
arObj=audiorecorder(fs, nbits, nChannels);
fprintf('Press any key to start %g seconds of recording...', duration);
pause;
fprintf('Recording...');
recordblocking(arObj, duration);
fprintf('Finished recording.\n');
fprintf('Press any key to play the recording...');
pause;
fprintf('\n');
play(arObj);
fprintf('Plotting the waveform...\n');
y=getaudiodata(arObj); % Get audio sample data
audiowrite('y_.wav',y,fs)
au=myAudioRead('y_.wav');
plot(y); % Plot the waveform
%%
%End Point Detction
figure;
opt=endPointDetect('defaultOpt'); 
opt.method='volZcr'; 
showPlot=1; 
endPoint=endPointDetect(au, opt, showPlot);
%%
%Remove Useless Data
figure;
j = 1;
for i = endPoint(1):endPoint(2)
    y_(j) = y(i);
    j = j+1;
end
plot(y_);
y = y_;
%%
%Volume
figure;
%audiowrite('y_.wav',y,fs)
%au=myAudioRead('y_.wav');
opt=wave2volume('defaultOpt');
opt.frameSize=256; opt.overlap=128;
time=(1:length(au.signal))/au.fs;
subplot(3,1,1);
plot(time, au.signal);
xlabel('Time (sec)');
ylabel('Amplitude');
title('waveFile');
subplot(3,1,2);
opt1=opt;
opt1.frame2volumeOpt.method='absSum';
volume1=wave2volume(au, opt1, 1);
ylabel(opt1.frame2volumeOpt.method);
subplot(3,1,3);
opt2=opt;
opt2.frame2volumeOpt.method='decibel';
volume2=wave2volume(au, opt2, 1);
ylabel(opt2.frame2volumeOpt.method);
fprintf('RMS Volume =  %g\n',rms(volume1));
%%
%Zero Crossing Error
figure;
frameSize=256;
overlap=0;
y=au.signal;
fs=au.fs;
frameMat=enframe(y, frameSize, overlap);
frameNum=size(frameMat, 2);
for i=1:frameNum
    frameMat(:,i)=frameMat(:,i)-mean(frameMat(:,i)); % mean justification
end
zcr=sum(frameMat(1:end-1, :).*frameMat(2:end, :)<0);
sampleTime=(1:length(y))/fs;
frameTime=((0:frameNum-1)*(frameSize-overlap)+0.5*frameSize)/fs;
subplot(2,1,1); plot(sampleTime, y); ylabel('Amplitude');
title('waveFile');
subplot(2,1,2);
plot(frameTime, zcr, '.-');
xlabel('Time (sec)');
ylabel('Count');
title('ZCR');
fprintf('RMS ZCR =  %g\n',rms(zcr));
%%
%Pitch
figure;
index1=11000;
index2=index1+frameSize-1;
frame=y(index1:index2);
subplot(2,1,1);
plot(y);
grid on
title('waveFile');
line(index1*[1 1], [-1 1], 'color', 'r');
line(index2*[1 1], [-1 1], 'color', 'r');
subplot(2,1,2);
plot(frame, '.-');
grid on
point=[7, 226];
line(point, frame(point), 'marker', 'o', 'color', 'red');
periodCount=6;
fp=((point(2)-point(1))/periodCount)/fs; % fundamental period (in sec)
ff=1/fp; % fundamental frequency (in Hz)
pitch=69+12*log2(ff/440); % pitch (in semitone)
fprintf('Fundamental period = %g second\n', fp);
fprintf('Fundamental frequency = %g Hertz\n', ff);
fprintf('Pitch = %g semitone\n', pitch);
%%
%GMM
%gmm = gmdistribution();
%%
    
%%
    
%%
    
%%
    
%%
    
%%
    
%%
    
%%
    
%%
    
%%
    
%%
    
%%